QT += quick

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

RESOURCES += qml.qrc \
    qtquickcontrols2.conf \
    sounds.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    qtquickcontrols2.conf \
    sounds/Duck-quack.mp3 \
    sounds/sound_1.wav \
    sounds/sound_10.wav \
    sounds/sound_11.wav \
    sounds/sound_12.wav \
    sounds/sound_13.wav \
    sounds/sound_14.wav \
    sounds/sound_15.wav \
    sounds/sound_16.wav \
    sounds/sound_2.wav \
    sounds/sound_3.wav \
    sounds/sound_4.wav \
    sounds/sound_5.wav \
    sounds/sound_6.wav \
    sounds/sound_7.wav \
    sounds/sound_8.wav \
    sounds/sound_9.wav
