import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts
import QtMultimedia


ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Digital Music-pad")


    property var songs: [
        "sounds/Duck-quack.mp3", "sounds/sound_1.wav", "sounds/sound_2.wav", "sounds/sound_3.wav",
        "sounds/sound_4.wav", "sounds/sound_5.wav", "sounds/sound_6.wav", "sounds/sound_7.wav",
        "sounds/sound_8.wav", "sounds/sound_9.wav", "sounds/sound_10.wav", "sounds/sound_11.wav",
        "sounds/sound_12.wav", "sounds/sound_13.wav", "sounds/sound_14.wav", "sounds/sound_15.wav",
        "sounds/sound_16.wav"
    ]

    function randomColors(){
        var arrayColor = []
        for(var i = 0; i < 16; i++){
            arrayColor.push(Qt.hsva(Math.random(), 1,(Math.random()*0.5)+0.5))
        }
        return arrayColor
    }

    property var buttonColors: randomColors()

    RowLayout {
        anchors.fill: parent
        anchors.margins: 20
        spacing: 5

        Column {
            Layout.fillHeight: true
            Layout.fillWidth: ture
            Slider {
                from: 0
                to: 100
                id: volumeSlider
                orientation: Qt.Vertical
                value: 0.5
            }

        }

        Grid {
            id: buttonGrid
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: 5
            columns: 4
            rows: 4



            Repeater {
                model: buttonGrid.rows * buttonGrid.columns
                Rectangle {
                    width: parent.width/buttonGrid.rows - (buttonGrid.spacing * buttonGrid.rows)
                    height: parent.height/buttonGrid.columns - (buttonGrid.spacing * buttonGrid.columns)
                    radius: width/10
                    color: buttonColors[index]

                    MediaPlayer {
                        id: ducksound
                        source: songs[index]

                        audioOutput: AudioOutput{
                            volume: volumeSlider.value/100
                        }
                    }

                    MouseArea{
                        anchors.fill: parent
                        onPressed: {
                            parent.color = parent.color.lighter()
                            ducksound.play()
                        }
                        onReleased: {
                            parent.color = buttonColors[index]
                        }
                    }
                }
            }
        }
    }
}

